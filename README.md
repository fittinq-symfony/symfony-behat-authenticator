# Symfony Behat Authenticator Bundle

The Symfony Behat Authenticator Bundle is a powerful tool that simplifies writing Behat tests for your Symfony applications. It provides Behat contexts and services to assist in testing user authentication, roles, and frontend interactions.

## Table of Contents

- [Introduction](#introduction)
- [Installation](#installation)
- [Usage](#usage)
- [Configuration](#configuration)

## Installation

You can install the bundle using Composer:

```bash
composer require fittinq/symfony-behat-authenticator
```

## Configuration

Update your project to include the Behat bundle in your test setup.
   ```yaml
    default:
       default:
       paths:
          - behat/features
       contexts:
        - Fittinq\Symfony\Behat\Authenticator\Context\AuthenticatorApiContext
        - Fittinq\Symfony\Behat\Authenticator\Context\AuthenticatorDatabaseContext
        - Fittinq\Symfony\Behat\Authenticator\Context\AuthenticatorUserContext
        - Fittinq\Symfony\Behat\Authenticator\Context\AuthenticatorRoleContext
        - Fittinq\Symfony\Behat\Authenticator\Context\AuthenticatorFrontendContext
   ```

## Usage

### AuthenticatorRoleContext

This context focuses on managing roles and includes steps for adding roles and verifying their existence.

Example Usage:
```gherkin
Given there are roles
  | name       |
  | ROLE_USER  |
  | ROLE_ADMIN |
  | ROLE_EDITOR|
```
### AuthenticatorUserContext

This context is designed for managing user-related Behat steps. It includes steps for adding users and authenticating them via an API.

Example Usage:
```gherkin
Given there are users
  | username | password | roles           |
  | user1    | secret   | ROLE_USER       |
  | admin    | password | ROLE_ADMIN      |
  | editor   | 123456   | ROLE_EDITOR     |
```

### AuthenticatorFrontendContext

The AuthenticatorFrontendContext is for testing frontend interactions, such as user logins and checking HTTP status codes and page content.

Example Usage:
```gherkin
Given user user1 logs in to the frontend app
Then the current page should contain text "Welcome, user1!"
```