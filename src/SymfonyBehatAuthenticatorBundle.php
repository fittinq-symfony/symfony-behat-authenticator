<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Authenticator;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SymfonyBehatAuthenticatorBundle extends Bundle
{
}