<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Authenticator\Service\User;

use Fittinq\Symfony\Behat\Authenticator\Service\DatabaseService;
use Fittinq\Symfony\Behat\Authenticator\Service\Role\RoleData;
use Symfony\Component\PasswordHasher\PasswordHasherInterface;

class UserService
{
    private PasswordHasherInterface $passwordHasher;

    /**
     * @var UserData[]
     */
    private array $users = [];
    private DatabaseService $databaseService;

    public function __construct(
        PasswordHasherInterface $passwordHasher,
        DatabaseService $databaseService
    )
    {
        $this->passwordHasher = $passwordHasher;
        $this->databaseService = $databaseService;
    }

    public function addUser(string $username, string $plainPassword, array $roleNames): void
    {
        $hashedPassword = $this->passwordHasher->hash($plainPassword);
        $roles = $this->getRoles($roleNames);
        $user = $this->createUser($username, $hashedPassword, $roles, $plainPassword);

        $this->users[] = $user;
    }

    /**
     * @return RoleData[]
     */
    private function getRoles(array $roleNames): array
    {
        $roles = [];
        foreach ($roleNames as $roleName) {
            $roleId = $this->databaseService->getIdBySecurityRoleName($roleName);

            $roleData = new RoleData();
            $roleData->setId($roleId);
            $roleData->setName($roleName);

            $roles[] = $roleData;
        }

        return $roles;
    }

    /**
     * @param RoleData[] $roles
     */
    private function createUser(string $username, string $passwordHashed, array $roles, string $password): UserData
    {
        $userId = $this->databaseService->insertUser($username, $passwordHashed);

        foreach ($roles as $role) {
            $this->databaseService->insertUserRoleRelation($userId, $role->getId());
        }

        $user = new UserData();
        $user->setId($userId);
        $user->setUsername($username);
        $user->setPlainPassword($password);
        $user->setHashedPassword($passwordHashed);
        $user->setToken(null);
        $user->setRoles($roles);

        return $user;
    }

    public function getUser(string $username): ?UserData
    {
        foreach ($this->users as $user)
        {
            if ($username === $user->getUsername()) {
                return $user;
            }
        }

        return null;
    }

    public function setToken(string $username, string $token): void
    {
        foreach ($this->users as $user)
        {
            if ($username === $user->getUsername()) {
                $user->setToken($token);
            }
        }
    }
}