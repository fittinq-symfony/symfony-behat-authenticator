<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Authenticator\Context;

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use Fittinq\Symfony\Behat\Authenticator\Service\Api\ApiService;
use Fittinq\Symfony\Behat\Authenticator\Service\User\UserService;

class AuthenticatorUserContext implements Context
{
    private UserService $userService;
    private ApiService $apiService;

    public function __construct(UserService $userService, ApiService $apiService)
    {
        $this->userService = $userService;
        $this->apiService = $apiService;
    }

    /**
     * @Given /^there are users$/
     */
    public function addUsers(TableNode $tableNode)
    {
        foreach ($tableNode as $item) {
            $roleNames = array_filter(explode(", ", $item['roles']));
            $this->userService->addUser($item['username'], $item['password'], $roleNames);
        }
    }

    /**
     * @When /^user (.*) authenticates via API$/
     */
    public function authenticateUser(string $username)
    {
        $user = $this->userService->getUser($username);
        $this->apiService->authenticate($user->getUsername(), $user->getPlainPassword());
    }

    /**
     * @When /^unknown user authenticates via API$/
     */
    public function authenticateUnknownUser()
    {
        $this->apiService->authenticate("unknown", "unknown");
    }
}