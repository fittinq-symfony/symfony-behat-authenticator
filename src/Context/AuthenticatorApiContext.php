<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Authenticator\Context;

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Fittinq\Symfony\Behat\Authenticator\Service\Api\ApiService;
use PHPUnit\Framework\Assert;

class AuthenticatorApiContext implements Context
{
    private ApiService $apiService;

    public function __construct(ApiService $apiService)
    {
        $this->apiService = $apiService;
    }

    /**
     * @When /^user (.*) makes an?o?t?h?e?r? (.*) request to ([^\/]*)\/(.*)$/
     */
    public function makeRequest(string $username, string $method, string $service, string $uri, PyStringNode $body = new PyStringNode([], 0))
    {
        $this->apiService->makeHttpRequest(
            $username,
            $method,
            $service,
            $uri,
            $body
        );
    }

    /**
     * @Then /^the HTTP status code should be (\d+)$/
     */
    public function assertHttpResponseCode(int $statusCode)
    {
        $response = $this->apiService->getHttpResponse();
        Assert::assertEquals($statusCode, $response->getStatusCode());
    }

    /**
     * @Then /^the authenticate request status code should be (\d+)$/
     */
    public function assertAuthenticationHttpResponseCode(int $statusCode)
    {
        $response = $this->apiService->getHttpAuthenticateResponse();
        Assert::assertEquals($statusCode, $response->getStatusCode());
    }
}
