<?php declare(strict_types=1);

namespace Fittinq\Symfony\Behat\Authenticator\Context;

use Behat\MinkExtension\Context\RawMinkContext;
use Fittinq\Symfony\Behat\Authenticator\Service\User\UserService;
use PHPUnit\Framework\Assert;

class AuthenticatorFrontendContext extends RawMinkContext
{
    private UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @Given /^(.*) logs in to frontend app$/
     */
    public function loginExsistingUser(string $username)
    {
        $user = $this->userService->getUser($username);

        $this->visitPath("/");

        $this->logIn($username, $user ? $user->getPlainPassword() : '');
    }

    private function logIn(string $username, string $password)
    {
        $this->getSession()->getPage()->find('css',"#username")->setValue($username);
        $this->getSession()->getPage()->find('css',"#password")->setValue($password);
        $this->getSession()->getPage()->find('css',"#submit-user-login")->click();
    }

    /**
     * @Then /^the current page has status code (\d+)$/
     */
    public function assertStatusCodeCurrentPage(int $statusCode)
    {
        Assert::assertStringContainsString((string) $statusCode, $this->getSession()->getPage()->getHtml());
    }

    /**
     * @Then /^the current page should contain text (.*)$/
     */
    public function assertPageContainsText(string $text)
    {
        Assert::assertStringContainsString($text, $this->getSession()->getPage()->getHtml());
    }
}